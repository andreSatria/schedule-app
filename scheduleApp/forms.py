from django import forms

class EntryForm(forms.Form):
    name = forms.CharField(max_length=100)
    date= forms.DateField(widget=forms.DateInput(attrs={'type': 'date'}))
    time = forms.TimeField(widget=forms.TimeInput(attrs={'type': 'time'}))
    description = forms.CharField(widget=forms.Textarea)
