from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from .models import Entry
from .forms import EntryForm

# Create your views here.
def index(request):
    return render(request,'scheduleApp/index.html')


@login_required
def schedule(request):
        entries = Entry.objects.filter(author=request.user)
        return render(request,'scheduleApp/schedule.html',{'entries':entries})

@login_required
def details(request, pk):
    entry = get_object_or_404(Entry, pk=pk)
    return render(request,'scheduleApp/details.html',{'entry':entry})

def add(request):
    if request.method == "POST":
        form = EntryForm(request.POST)

        if form.is_valid():
            name =  form.cleaned_data['name']
            date =  form.cleaned_data['date']
            time =  form.cleaned_data['time']
            description =  form.cleaned_data['description']

            Entry.objects.create(
                name=name,
                author=request.user,
                date=date,
                time=time,
                description=description,
            ).save()


            return HttpResponseRedirect('/schedule')
    else:
        form = EntryForm()

    return render(request,'scheduleApp/form.html',{'form':form})

@login_required
def delete(request, pk):

    if request.method == 'DELETE':
        entry = get_object_or_404(Entry, pk=pk)
        entry.delete()

    return HttpResponseRedirect('/')

@login_required
def deleteAll(request):
    Entry.objects.all().delete()
    return HttpResponseRedirect('/schedule')

def signup(request):

    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('/schedule')

    else:
        form = UserCreationForm()

    return render(request, 'registration/signup.html', {'form': form})
