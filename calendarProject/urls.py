"""calendarProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path
from scheduleApp import views

urlpatterns = [
    path('',views.index,name='index'),
    path('schedule',views.schedule,name='schedule'),
    path('signup',views.signup,name="signup"),
    path('login/',auth_views.LoginView.as_view(),name='login'),
    path('logout/',auth_views.LogoutView.as_view(),name='logout'),
    path('_schedule_card/<int:pk>', views.details, name='details'),
    path('_schedule_card/add',views.add,name="add"),
    path('_schedule_card/',views.deleteAll,name="deleteAll"),
    path('_schedule_card/delete/<int:pk>',views.delete,name="delete"),
    path('admin/', admin.site.urls),
]
